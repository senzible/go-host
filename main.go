package main

import (
	"log"
	"net/http"
	"os"
	"path"
)

func main() {
	err := http.ListenAndServe(":5000", handleSPA())
	if err != nil {
		log.Fatal("failed to listen and serve", err)
	}
}

func handleSPA() http.Handler {
	return http.FileServer(&indexWrapper{http.Dir("spa")})
}

type indexWrapper struct {
	assets http.FileSystem
}

func (i *indexWrapper) Open(name string) (http.File, error) {
	log.Print(name)
	ret, err := i.assets.Open(name)
	if !os.IsNotExist(err) || path.Ext(name) != "" {
		return ret, err
	}
	return i.assets.Open("index.html")
}
