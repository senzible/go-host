# STEP 1 build executable binary
FROM golang:1.11-alpine as gobuilder
# Create appuser
RUN adduser -D -g '' appuser
WORKDIR /src
COPY . .
#get dependancies
RUN go mod download
#build the binary
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -installsuffix cgo -o /go-host

# STEP 2 build a small image
# start from scratch
FROM scratch
COPY --from=gobuilder /etc/passwd /etc/passwd
# Copy our static executable
COPY --from=gobuilder /go-host .
COPY --from=gobuilder /src/spa /spa
USER appuser
EXPOSE 5000
ENTRYPOINT ["/go-host"]